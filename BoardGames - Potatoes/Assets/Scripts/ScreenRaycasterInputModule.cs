﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenRaycasterInputModule : MonoBehaviour
{

    [SerializeField]
    private ScreenPointerInputSystem screenPointerInputSystem;

    [SerializeField]
    private Camera mainCamera;

    public delegate void ScreenPointerAction(GameObject inspectedObj, Vector3 hitWorldPosition);

    public static event ScreenPointerAction OnPointerHoverEnter;
    public static event ScreenPointerAction OnPointerHoverStay;
    public static event ScreenPointerAction OnPointerHoverExit;

    public static event ScreenPointerAction OnPointerClickBegin;
    public static event ScreenPointerAction OnPointerClickHold;
    public static event ScreenPointerAction OnPointerClickEnd;
    public static event ScreenPointerAction OnPointerClickCancel;

    //private Dictionary<int, List<GameObject>> firstInspectedObjects = new Dictionary<int, List<GameObject>>();
    //private Dictionary<int, List<GameObject>> stillInspectedObjects = new Dictionary<int, List<GameObject>>();
    //private Dictionary<int, List<GameObject>> inactiveInspectedObjects = new Dictionary<int, List<GameObject>>();

    //private Dictionary<int, List<GameObject>> justClickedObjects = new Dictionary<int, List<GameObject>>();
    //private Dictionary<int, List<GameObject>> stillClickedObjects = new Dictionary<int, List<GameObject>>();
    //private Dictionary<int, List<GameObject>> endClickedObjects = new Dictionary<int, List<GameObject>>();

    private Dictionary<int, List<GameObject>> lastHoveredObjects = new Dictionary<int, List<GameObject>>();
    private Dictionary<int, PointerClickObjects> lastClickedObjects = new Dictionary<int, PointerClickObjects>();

    //private List<GameObject> lastHittedGameObjs = new List<GameObject>();
    //private RaycastHit[] hittedObjs;
    //private Ray mouseRay;

    //private List<GameObject> uncheckedHittedGameObjs = new List<GameObject>();

    protected virtual void OnEnable()
    {
        ScreenPointerInputSystem.OnPointerStart += HandlePointerBegin; ;
        ScreenPointerInputSystem.OnPointerChange += UpdateObjectsHoverState;
        ScreenPointerInputSystem.OnPointerEnd += HandlePointerEnd;

        ScreenPointerInputSystem.OnPointerClickChange += UpdateObjectsClickState;
    }

    protected virtual void OnDisable()
    {
        ScreenPointerInputSystem.OnPointerStart -= HandlePointerBegin; ;
        ScreenPointerInputSystem.OnPointerChange -= UpdateObjectsHoverState;
        ScreenPointerInputSystem.OnPointerEnd -= HandlePointerEnd;

        ScreenPointerInputSystem.OnPointerClickChange -= UpdateObjectsClickState;
    }

    private void HandlePointerBegin(PointerEventData pointerEventData)
    {
        lastHoveredObjects.Add(pointerEventData.ID, new List<GameObject>());
        lastClickedObjects.Add(pointerEventData.ID, new PointerClickObjects());
    }

    private void HandlePointerEnd(PointerEventData pointerEventData)
    {
        lastHoveredObjects.Remove(pointerEventData.ID);
        lastClickedObjects.Remove(pointerEventData.ID);
    }

    private void UpdateObjectsHoverState(PointerEventData pointerEventData)
    {
        List<GameObject> currentlyPointerHoveredObjects = GetSceneObjectsOverPointer(pointerEventData);
        List<GameObject> previouslyPointerHoveredObjects = lastHoveredObjects[pointerEventData.ID];
        List<GameObject> pointerObjectsHoverEnd = new List<GameObject>(previouslyPointerHoveredObjects);

        for (int i = 0; i < currentlyPointerHoveredObjects.Count; i++)
        {
            GameObject currentGameObject = currentlyPointerHoveredObjects[i];

            SendObjectHoverActiveEvent(currentGameObject, previouslyPointerHoveredObjects);

            if (pointerObjectsHoverEnd.Contains(currentGameObject) == true)
            {
                pointerObjectsHoverEnd.Remove(currentGameObject);
            }
        }

        SendHoverInactiveEvent(pointerObjectsHoverEnd);

        lastHoveredObjects[pointerEventData.ID] = currentlyPointerHoveredObjects;
    }

    private void SendObjectHoverActiveEvent(GameObject gameObject, List<GameObject> lastActiveObjects)
    {
        if (lastActiveObjects.Contains(gameObject) == false)
        {
            OnPointerHoverEnter(gameObject, Input.mousePosition);
        }
        else
        {
            OnPointerHoverStay(gameObject, Input.mousePosition);
        }
    }

    private void SendHoverInactiveEvent(List<GameObject> hoverEndObjects)
    {
        for (int i = 0; i < hoverEndObjects.Count; i++)
        {
            OnPointerHoverExit(hoverEndObjects[i], Input.mousePosition);
        }
    }

    private List<GameObject> GetSceneObjectsOverPointer(PointerEventData pointerEventData)
    {
        List<GameObject> result = new List<GameObject>();

        Ray mouseRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit[] objectsOverPointer = Physics.RaycastAll(mouseRay);
        RaycastHit2D[] objects2DOverPointer = Physics2D.RaycastAll(pointerEventData.ScreenPosition, Vector2.zero);

        for (int i = 0; i < objectsOverPointer.Length; ++i)
        {
            result.Add(objectsOverPointer[i].transform.gameObject);
        }

        for (int i = 0; i < objects2DOverPointer.Length; ++i)
        {
            result.Add(objects2DOverPointer[i].transform.gameObject);
        }

        return result;
    }

    private void UpdateObjectsClickState(PointerClickEventData pointerClickEventData)
    {
        List<GameObject> currentlyPointerHoveredObjects = lastHoveredObjects[pointerClickEventData.TargetPointerEventData.ID];
        PointerClickObjects previouslyPointerClickedObjects = lastClickedObjects[pointerClickEventData.TargetPointerEventData.ID];

        if (pointerClickEventData.State == PointerClickEventData.PointerClickState.BEGIN)
        {
            SendObjectsClickBeginEvent(previouslyPointerClickedObjects, pointerClickEventData.ClickID, currentlyPointerHoveredObjects);
        }
        else
        {
            UpdateObjectsPointerClickState(previouslyPointerClickedObjects, pointerClickEventData.ClickID, pointerClickEventData.State, currentlyPointerHoveredObjects);
        }
    }

    private void SendObjectsClickBeginEvent(PointerClickObjects pointerClickObjects, int clickID, List<GameObject> hoveredObjects)
    {
        pointerClickObjects.AddObjectsForClick(clickID, hoveredObjects);

        for (int i = 0; i < hoveredObjects.Count; ++i)
        {
            OnPointerClickBegin(hoveredObjects[i], Input.mousePosition);
        }
    }

    private void UpdateObjectsPointerClickState(PointerClickObjects pointerClickObjects, int clickID, PointerClickEventData.PointerClickState clickState, List<GameObject> hoveredObjects)
    {
        List<GameObject> previouslyClickedObjects = pointerClickObjects.GetObjectsForClick(clickID);

        if (clickState == PointerClickEventData.PointerClickState.HOLD)
        {
            for (int i = 0; i < hoveredObjects.Count; ++i)
            {
                if (previouslyClickedObjects.Contains(hoveredObjects[i]) == true)
                {
                    OnPointerClickHold(hoveredObjects[i], Input.mousePosition);
                }
            }
        }
        else if (clickState == PointerClickEventData.PointerClickState.END)
        {
            List<GameObject> clickCancelObjects = new List<GameObject>(previouslyClickedObjects);

            for (int i = 0; i < hoveredObjects.Count; ++i)
            {
                if (clickCancelObjects.Contains(hoveredObjects[i]) == true)
                {
                    OnPointerClickEnd(hoveredObjects[i], Input.mousePosition);

                    clickCancelObjects.Remove(hoveredObjects[i]);
                }
            }

            SendObjectsClickCancelEvent(clickCancelObjects);

            pointerClickObjects.RemoveClickObjects(clickID);
        }
    }

    private void SendObjectsClickCancelEvent(List<GameObject> activeObjects)
    {
        for (int i = 0; i < activeObjects.Count; ++i)
        {
            OnPointerClickCancel(activeObjects[i], Input.mousePosition);
        }
    }

    private class PointerClickObjects
    {
        private Dictionary<int, List<GameObject>> ClickIDToObject
        {
            get; set;
        }

        public PointerClickObjects()
        {
            ClickIDToObject = new Dictionary<int, List<GameObject>>();
        }

        public void AddObjectsForClick(int clickID, List<GameObject> gameObjects)
        {
            ClickIDToObject.Add(clickID, gameObjects);
        }

        public List<GameObject> GetObjectsForClick(int clickID)
        {
            return (ClickIDToObject.ContainsKey(clickID) == true) ? ClickIDToObject[clickID] : null;
        }

        public void RemoveClickObjects(int clickID)
        {
            ClickIDToObject.Remove(clickID);
        }
    }
}
