﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class Potatoe : MonoBehaviour {

    public enum PotatoeState
    {
        Empty,
        Open,
        Closed
    }

    [SerializeField]
    private Sprite potatoeSprite_open;
    [SerializeField]
    private Sprite potatoeSprite_closed;

    [SerializeField]
    private Color spriteNormalColor = Color.white;

    [SerializeField]
    private Color spriteHoverColor = Color.white;

    [SerializeField]
    private Color spriteClickedColor = Color.white;

    [ReadOnly, SerializeField]
    private SpriteRenderer spriteRenderer;

    [ReadOnly, SerializeField]
    private PotatoeState state = PotatoeState.Open;
    public PotatoeState State
    {
        get
        {
            return state;
        }
        private set
        {
            state = value;
        }
    }

    private bool Hovered
    {
        get;
        set;
    }

    private bool Clicked
    {
        get;
        set;
    }

    protected virtual void Awake()
    {
        Hovered = false;
        Clicked = false;
    }

    protected virtual void OnEnable()
    {
        ScreenRaycasterInputModule.OnPointerClickBegin += HandleOnPointerClickBegin;
        ScreenRaycasterInputModule.OnPointerClickEnd += HandleOnPointerClickEnd;
        ScreenRaycasterInputModule.OnPointerClickCancel += HandleOnPointerClickCancel;
        ScreenRaycasterInputModule.OnPointerHoverEnter += HandleOnPointerHoverEnter;
        ScreenRaycasterInputModule.OnPointerHoverExit += HandleOnPointerHoverExit;
    }

    protected virtual void OnDisable()
    {
        ScreenRaycasterInputModule.OnPointerClickBegin -= HandleOnPointerClickBegin;
        ScreenRaycasterInputModule.OnPointerClickEnd -= HandleOnPointerClickEnd;
        ScreenRaycasterInputModule.OnPointerClickCancel -= HandleOnPointerClickCancel;
        ScreenRaycasterInputModule.OnPointerHoverEnter -= HandleOnPointerHoverEnter;
        ScreenRaycasterInputModule.OnPointerHoverExit -= HandleOnPointerHoverExit;
    }

    private void HandleOnPointerHoverEnter(GameObject inspectedObj, Vector3 hitPoint)
    {
        if (inspectedObj == gameObject && spriteRenderer != null)
        {
            spriteRenderer.color = spriteHoverColor;

            Hovered = true;
        }
    }

    private void HandleOnPointerHoverExit(GameObject inspectedObj, Vector3 hitPoint)
    {
        if (inspectedObj == gameObject && spriteRenderer != null)
        {
            spriteRenderer.color = (Clicked == true) ? spriteClickedColor : spriteNormalColor;

            Hovered = false;
        }
    }

    private void HandleOnPointerClickBegin(GameObject inspectedObj, Vector3 hitPoint)
    {
        if (inspectedObj == gameObject && spriteRenderer != null)
        {
            spriteRenderer.color = spriteClickedColor;

            Clicked = true;
        }
    }

    private void HandleOnPointerClickEnd(GameObject inspectedObj, Vector3 hitPoint)
    {
        if (inspectedObj == gameObject && spriteRenderer != null)
        {
            spriteRenderer.color = (Hovered == true) ? spriteHoverColor : spriteNormalColor;

            ToggleState();

            Clicked = false;
        }
    }

    private void HandleOnPointerClickCancel(GameObject inspectedObj, Vector3 hitPoint)
    {
        if (inspectedObj == gameObject && spriteRenderer != null)
        {
            spriteRenderer.color = (Hovered == true) ? spriteHoverColor : spriteNormalColor;

            Clicked = false;
        }
    }

    private void Reset()
    {
        CacheSpriteRenderer();
    }

    private void OnValidate()
    {
        CacheSpriteRenderer();
    }

    private void CacheSpriteRenderer()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void ToggleState()
    {
        if (State == PotatoeState.Closed || spriteRenderer == null)
        {
            return;
        }

        spriteRenderer.sprite = potatoeSprite_closed;
        State = PotatoeState.Closed;
    }
}
