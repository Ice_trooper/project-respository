﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardManager : MonoBehaviour {

    public enum BoardShape
    {
        Rectangle,
        Triangle,
        Diamond
    }

    [SerializeField]
    private GameObject potatoePrefab;

    [SerializeField]
    private BoardShape shape;

    [SerializeField]
    private float spaceBetweenPotatoes;

    [SerializeField]
    private GameObject[,] boardArray;

    [SerializeField]
    private int boardRowCount;
    public int BoardRowCount
    {
        get
        {
            return boardRowCount;
        }
        set
        {
            if(value<0)
            {
                boardRowCount = 0; //TODO Exception?
            }
            else
            {
                boardRowCount = value;
            }
        }
    }

    [SerializeField]
    private int boardColsCount;
    public int BoardColsCount
    {
        get
        {
            return boardColsCount;
        }
        set
        {
            if (value < 0)
            {
                boardColsCount = 0; //TODO Exception?
            }
            else
            {
                boardColsCount = value;
            }
        }
    }

    private void Start ()
    {
        GenerateBoard(shape);
	}

    private void GenerateBoard(BoardShape boardShape)
    {
        switch(boardShape)
        {
            case BoardShape.Rectangle:
                GenerateRectangleBoard(BoardRowCount, BoardColsCount);
                break;
            case BoardShape.Triangle:
                GenerateTriangleBoard(BoardColsCount);
                break;
            case BoardShape.Diamond:
                GenerateDiamondBoard(BoardColsCount);
                break;
            default:
                break;
        }
    }

    private void GenerateRectangleBoard(int rows, int cols)
    {
        BoardRowCount = rows;
        BoardColsCount = cols;
        boardArray = new GameObject[BoardRowCount, BoardColsCount];

        Vector2 leftLowerCellPos = CalculateLeftLowerCellPos(BoardRowCount, BoardColsCount);
        Debug.Log(leftLowerCellPos);
        Vector2 newPosition = leftLowerCellPos;
        GameObject newPotatoe;

        for (int i = 0; i < rows; i++)
        {
            for(int j = 0; j < cols; j++)
            {
                newPotatoe = Instantiate(potatoePrefab, newPosition, Quaternion.identity, transform);
                //newPotatoe.GetComponent<Potatoe>().State = Potatoe.PotatoeState.Open;
                boardArray[i, j] = newPotatoe;
                newPosition.x += spaceBetweenPotatoes;
            }
            newPosition.x = leftLowerCellPos.x;
            newPosition.y += spaceBetweenPotatoes;
        }
    }

    private void GenerateTriangleBoard(int cols)
    {
        BoardRowCount = (cols + 1) / 2;
        BoardColsCount = cols;
        boardArray = new GameObject[BoardRowCount, BoardColsCount];

        Vector2 leftLowerCellPos = CalculateLeftLowerCellPos(BoardRowCount, BoardColsCount);
        Vector2 newPosition = leftLowerCellPos;
        GameObject newPotatoe;

        for(int i = 0; i < BoardRowCount; i++)
        {
            for(int j = 0; j < BoardColsCount; j++)
            {
                if (j >= i && j < BoardColsCount - i)
                {
                    newPotatoe = Instantiate(potatoePrefab, newPosition, Quaternion.identity, transform);
                    boardArray[i, j] = newPotatoe;
                }
                newPosition.x += spaceBetweenPotatoes;
            }
            newPosition.x = leftLowerCellPos.x;
            newPosition.y += spaceBetweenPotatoes;
        }
    }

    private void GenerateDiamondBoard(int cols)
    {
        BoardRowCount = cols;
        BoardColsCount = cols;
        boardArray = new GameObject[BoardRowCount, BoardColsCount];

        Vector2 leftLowerCellPos = CalculateLeftLowerCellPos(BoardRowCount, BoardColsCount);
        Vector2 newPosition = leftLowerCellPos;
        GameObject newPotatoe;

        for (int i = 0; i < BoardRowCount; i++)
        {
            for (int j = 0; j < BoardColsCount; j++)
            {
                if(i < BoardRowCount/2)
                {
                    if (j >= BoardRowCount/2-i && j <= BoardRowCount/2+i)
                    {
                        newPotatoe = Instantiate(potatoePrefab, newPosition, Quaternion.identity, transform);
                        //newPotatoe.GetComponent<Potatoe>().State = Potatoe.PotatoeState.Open;
                        boardArray[i, j] = newPotatoe;
                    }
                }
                else
                {
                    if (j >= i-BoardRowCount/2 && j < BoardColsCount - (i-BoardRowCount/2))
                    {
                        newPotatoe = Instantiate(potatoePrefab, newPosition, Quaternion.identity, transform);
                        //newPotatoe.GetComponent<Potatoe>().State = Potatoe.PotatoeState.Open;
                        boardArray[i, j] = newPotatoe;
                    }
                }
                newPosition.x += spaceBetweenPotatoes;
            }
            newPosition.x = leftLowerCellPos.x;
            newPosition.y += spaceBetweenPotatoes;
        }
    }

    private Vector2 CalculateLeftLowerCellPos(int rows, int cols)
    {
        float x_pos = transform.position.x - (cols * spaceBetweenPotatoes / 2) + (spaceBetweenPotatoes / 2);
        float y_pos = transform.position.y - (rows * spaceBetweenPotatoes / 2) + (spaceBetweenPotatoes / 2);
        return new Vector2(x_pos, y_pos);
    }
}
