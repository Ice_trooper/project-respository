﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointerClickEventData
{
    public PointerEventData TargetPointerEventData {
        get;
        private set;
    }

    public int ClickID {
        get;
        private set;
    }

    public PointerClickState State {
        get;
        private set;
    }

    public enum PointerClickState
    {
        UNKNOWN,
        BEGIN,
        HOLD,
        END
    }

    public static PointerClickEventData Create(PointerEventData targetPointerEventData, int clickID, PointerClickState state)
    {
        PointerClickEventData result = new PointerClickEventData();

        result.TargetPointerEventData = targetPointerEventData;
        result.ClickID = clickID;
        result.State = state;

        return result;
    }

}
