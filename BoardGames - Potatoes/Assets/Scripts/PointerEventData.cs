﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointerEventData
{
    public int ID {
        get;
        private set;
    }

    public float BeginTime {
        get;
        private set;
    }

    public float DeltaTime {
        get;
        private set;
    }

    public Vector2 ScreenPosition {
        get;
        private set;
    }

    public Vector2 DeltaPosition {
        get;
        private set;
    }

    public PointerState State {
        get;
        private set;
    }

    public static PointerEventData Create(int id, PointerState state, float beginTime, Vector2 screenPosition)
    {
        PointerEventData result = new PointerEventData();

        result.ID = id;
        result.State = state;
        result.BeginTime = beginTime;
        result.ScreenPosition = screenPosition;

        return result;
    }

    public PointerEventData SetDeltaTime(float deltaTime)
    {
        DeltaTime = deltaTime;

        return this;
    }

    public PointerEventData SetDeltaPosition(Vector2 deltaPosition)
    {
        DeltaPosition = deltaPosition;

        return this;
    }

    public enum PointerState
    {
        BEGIN,
        STAY,
        END
    }

}
