﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseScreenPointerDataProvider
{
    public abstract void CheckPointers();

    public abstract List<PointerEventData> GetPointersData();

    public abstract List<PointerClickEventData> GetPointersClickData();
}
