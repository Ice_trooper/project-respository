﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseScreenPointerDataProvider : BaseScreenPointerDataProvider
{
    private static int RECOGNIZING_BUTTONS_COUNT = 2;

    private Dictionary<int, PointerClickEventData.PointerClickState> ButtonIDToState {
        get;
        set;
    }

    private float LastCheckTime {
        get;
        set;
    }

    private Vector2 LastScreenPosition {
        get;
        set;
    }

    private bool WasFirstEvent {
        get;
        set;
    }

    private PointerEventData MousePointerEventData = null;

    public MouseScreenPointerDataProvider()
    {
        LastCheckTime = Time.realtimeSinceStartup;
        LastScreenPosition = Vector2.zero;
        WasFirstEvent = false;

        ButtonIDToState = new Dictionary<int, PointerClickEventData.PointerClickState>();

        for (int i = 0; i < RECOGNIZING_BUTTONS_COUNT; ++i)
        {
            ButtonIDToState.Add(i, PointerClickEventData.PointerClickState.UNKNOWN);
        }
    }

    public override void CheckPointers()
    {
        if (Input.mousePresent == true)
        {
            CheckMouse();
            CheckMouseButtons();

            WasFirstEvent = true;
        }
    }

    public override List<PointerEventData> GetPointersData()
    {
        List<PointerEventData> result = new List<PointerEventData>();

        if (MousePointerEventData != null)
        {
            result.Add(MousePointerEventData);
        }

        return result;
    }

    public override List<PointerClickEventData> GetPointersClickData()
    {
        List<PointerClickEventData> result = new List<PointerClickEventData>();

        foreach (KeyValuePair<int, PointerClickEventData.PointerClickState> dictionaryRecord in ButtonIDToState)
        {
            if (dictionaryRecord.Value != PointerClickEventData.PointerClickState.UNKNOWN)
            {
                result.Add(PointerClickEventData.Create(MousePointerEventData, dictionaryRecord.Key, dictionaryRecord.Value));
            }
        }

        return result;
    }

    private void CheckMouse()
    {
        if (Input.mousePresent == false)
        {
            return;
        }

        Vector2 currentPosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        float currentTime = Time.realtimeSinceStartup;

        PointerEventData.PointerState state = (WasFirstEvent == true) ? PointerEventData.PointerState.STAY : PointerEventData.PointerState.BEGIN;

        MousePointerEventData = PointerEventData.Create(0, state, 0.0f, Input.mousePosition);
        MousePointerEventData.SetDeltaPosition(currentPosition - LastScreenPosition);
        MousePointerEventData.SetDeltaTime(currentTime - LastCheckTime);

        LastScreenPosition = currentPosition;
        LastCheckTime = currentTime;
    }

    private void CheckMouseButtons()
    {
        for (int i = 0; i < RECOGNIZING_BUTTONS_COUNT; ++i)
        {
            if (Input.GetMouseButtonDown(i) == true)
            {
                ButtonIDToState[i] = PointerClickEventData.PointerClickState.BEGIN;
            }
            else if (Input.GetMouseButton(i) == true)
            {
                ButtonIDToState[i] = PointerClickEventData.PointerClickState.HOLD;
            }

            if (Input.GetMouseButtonUp(i) == true)
            {
                ButtonIDToState[i] = PointerClickEventData.PointerClickState.END;
            }

            if (Input.GetMouseButtonUp(i) == false && ButtonIDToState[i] == PointerClickEventData.PointerClickState.END)
            {
                ButtonIDToState[i] = PointerClickEventData.PointerClickState.UNKNOWN;
            }
        }
    }
}
