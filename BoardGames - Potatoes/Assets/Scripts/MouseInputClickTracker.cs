﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseInputClickTracker : MonoBehaviour
{

    /*private int TrackingButtonID
    {
        get; set;
    }

    private Vector2 BeginPosition
    {
        get; set;
    }

    private Vector2 LastPosition
    {
        get; set;
    }

    public MouseInputClickTracker(int buttonID)
    {
        TrackingButtonID = buttonID;
        BeginPosition = Input.mousePosition;
        LastPosition = Input.mousePosition;
    }

    public bool IsActive()
    {
        return Input.GetMouseButtonDown(TrackingButtonID) == true || Input.GetMouseButton(TrackingButtonID) == true || Input.GetMouseButtonUp(TrackingButtonID) == true;
    }

    public PointerClickEventData GetNextEvent()
    {
        if (IsActive() == false)
        {
            return null;
        }

        Vector2 currentPosition = Input.mousePosition;

        PointerClickEventData newEventData = new PointerClickEventData(0, Input.mousePosition, LastPosition - currentPosition, 0.0f, 0.0f);
        newEventData.State = DeterminePointerState();
        LastPosition = Input.mousePosition;

        return newEventData;
    }

    private PointerClickEventData.PointerClickState DeterminePointerState()
    {
        if (Input.GetMouseButtonDown(TrackingButtonID) == true)
        {
            return PointerClickEventData.PointerClickState.BEGIN;
        }
        else if (Input.GetMouseButton(TrackingButtonID) == true)
        {
            return PointerClickEventData.PointerClickState.HOLD;
        }
        else if (Input.GetMouseButtonUp(TrackingButtonID) == true)
        {
            return PointerClickEventData.PointerClickState.END;
        }

        return PointerClickEventData.PointerClickState.UNKNOWN;
    }*/
}
