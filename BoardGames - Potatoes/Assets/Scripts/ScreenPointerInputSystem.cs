﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ScreenPointerInputSystem : MonoBehaviour
{
    public static Action<PointerEventData> OnPointerStart = delegate { };
    public static Action<PointerEventData> OnPointerChange = delegate { };
    public static Action<PointerEventData> OnPointerEnd = delegate { };

    public static Action<PointerClickEventData> OnPointerClickChange = delegate { };

    private List<BaseScreenPointerDataProvider> PointerDataProviders = new List<BaseScreenPointerDataProvider>();

    protected virtual void Awake()
    {
        PointerDataProviders.Add(new MouseScreenPointerDataProvider());
    }

    protected virtual void Start()
    {

    }

    protected virtual void Update()
    {
        UpdatePointerDataProviders();
    }

    private void UpdatePointerDataProviders()
    {
        UpdateProviders();
        SendProvidersPointerData();
        SendProvidersPointerClickData();
    }

    private void UpdateProviders()
    {
        for (int i = 0; i < PointerDataProviders.Count; ++i)
        {
            PointerDataProviders[i].CheckPointers();
        }
    }

    private void SendProvidersPointerData()
    {
        for (int i = 0; i < PointerDataProviders.Count; ++i)
        {
            List<PointerEventData> pointersData = PointerDataProviders[i].GetPointersData();

            SendPointerChangeEvent(pointersData);
        }
    }

    private void SendProvidersPointerClickData()
    {
        for (int i = 0; i < PointerDataProviders.Count; ++i)
        {
            List<PointerClickEventData> pointersClickData = PointerDataProviders[i].GetPointersClickData();

            SendPointerClickChangeEvent(pointersClickData);
        }
    }

    private void SendPointerChangeEvent(List<PointerEventData> events)
    {
        for (int i = 0; i < events.Count; ++i)
        {
            switch (events[i].State)
            {
                case PointerEventData.PointerState.BEGIN:
                    OnPointerStart(events[i]);
                    break;
                case PointerEventData.PointerState.STAY:
                    OnPointerChange(events[i]);

                    break;
                case PointerEventData.PointerState.END:
                    OnPointerEnd(events[i]);
                    break;
            }
        }
    }

    private void SendPointerClickChangeEvent(List<PointerClickEventData> events)
    {
        for (int i = 0; i < events.Count; ++i)
        {
            OnPointerClickChange(events[i]);
        }
    }
}
