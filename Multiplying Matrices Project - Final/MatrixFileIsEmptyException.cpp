#include "MatrixException.cpp"

using namespace std;

class MatrixFileIsEmptyException:public MatrixException
{
public:
    MatrixFileIsEmptyException(): MatrixException(MSG_FILE_IS_EMPTY_EXCEPTION){}
};
