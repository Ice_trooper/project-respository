#include "MatrixException.cpp"

using namespace std;

class MatrixBadReadSizeException:public MatrixException
{
public:
    MatrixBadReadSizeException(): MatrixException(MSG_BAD_READ_SIZE_EXCEPTION){}
};
