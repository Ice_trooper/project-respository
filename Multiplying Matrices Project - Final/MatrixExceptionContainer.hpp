#ifndef MATRIXEXCEPTIONCONTAINER_HPP_INCLUDED
#define MATRIXEXCEPTIONCONTAINER_HPP_INCLUDED

#include "MatrixNotInitializedException.cpp"
#include "MatrixBadSizeException.cpp"
#include "MatrixAlreadyCreatedException.cpp"
#include "MatrixFileIsEmptyException.cpp"
#include "MatrixBadReadSizeException.cpp"
#include "MatrixBadReadMatrixValuesException.cpp"
#include "MatrixCannotReadEntireFileException.cpp"
#include "MatrixCannotOpenFileException.cpp"
#include "MatrixMultiplyFormatException.cpp"
#include "MatrixMultiplyEmptyException.cpp"
#include "MatrixGenerateMinBiggerThanMaxException.cpp"

#endif // MATRIXEXCEPTIONCONTAINER_HPP_INCLUDED
