#ifndef MATRIXEXCEPTION_HPP_INCLUDED
#define MATRIXEXCEPTION_HPP_INCLUDED

using namespace std;

class MatrixException:public exception
{
public:
    MatrixException(){};
    MatrixException(const string& message);

    virtual const char* what() const noexcept;

private:
    string mtxExMessage;
};


#endif // MATRIXEXCEPTION_HPP_INCLUDED
