#include "MatrixException.cpp"

using namespace std;

class MatrixMultiplyEmptyException:public MatrixException
{
public:
    MatrixMultiplyEmptyException(): MatrixException(MSG_MULTIPLY_EMPTY_EXCEPTION){}
};
