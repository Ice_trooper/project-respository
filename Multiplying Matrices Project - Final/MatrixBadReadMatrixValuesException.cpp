#include "MatrixException.cpp"

using namespace std;

class MatrixBadReadMatrixValuesException:public MatrixException
{
public:
    MatrixBadReadMatrixValuesException(): MatrixException(MSG_BAD_READ_MATRIX_VALUES_EXCEPTION){}
};
