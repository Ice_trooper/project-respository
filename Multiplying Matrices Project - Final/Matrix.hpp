#ifndef MATRIX_HPP_INCLUDED
#define MATRIX_HPP_INCLUDED

using namespace std;

class Matrix
{
public:
    Matrix();
    Matrix(Matrix& mtx);
    ~Matrix();
    Matrix& operator= (const Matrix& mtx);
    unsigned int *getMatrixSize();

    void displayMatrix();
    void createEmptyMatrixArray(unsigned int mSize[]);
    void createEmptyMatrixArray(unsigned int m_x, unsigned int m_y);
    void deleteMatrixArray();
    void generateMatrixArray(unsigned int mSize[], int minimum, int maximum);
    void generateMatrixArray(unsigned int mSize[], float minimum, float maximum);

    void readFromFile(string fileName);
    void writeToFile(string fileName);
    //bool assignNumbersToMatrixArray();

    Matrix multiplyBy(Matrix& matrix); //A(nXm)*B(mXp)=C(nXp)
    Matrix multiplyBy(float number);

private:
    unsigned int matrixSize[2];
    float **matrixArray;
    void setMatrixSize(unsigned int m_x, unsigned int m_y);
};

#endif // MATRIX_HPP_INCLUDED
