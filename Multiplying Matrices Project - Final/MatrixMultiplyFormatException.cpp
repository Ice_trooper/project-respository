#include "MatrixException.cpp"

using namespace std;

class MatrixMultiplyFormatException:public MatrixException
{
public:
    MatrixMultiplyFormatException(): MatrixException(MSG_MULTIPLY_FORMAT_EXCEPTION){}
};
