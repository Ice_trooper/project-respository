#include "MatrixException.cpp"

using namespace std;

class MatrixBadSizeException:public MatrixException
{
public:
    MatrixBadSizeException(): MatrixException(MSG_BAD_SIZE_EXCEPTION){}
};
