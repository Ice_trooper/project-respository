#include "MatrixException.cpp"

using namespace std;

class MatrixCannotOpenFileException:public MatrixException
{
public:
    MatrixCannotOpenFileException(): MatrixException(MSG_CANNOT_OPEN_FILE_EXCEPTION){}
};
