#include "MatrixException.cpp"

using namespace std;

class MatrixAlreadyCreatedException:public MatrixException
{
public:
    MatrixAlreadyCreatedException(): MatrixException(MSG_ALREADY_CREATED_EXCEPTION){}
};
