#include "MatrixException.cpp"

using namespace std;

class MatrixCannotReadEntireFileException:public MatrixException
{
public:
    MatrixCannotReadEntireFileException(): MatrixException(MSG_CANNOT_READ_ENTIRE_FILE_EXCEPTION){}
};
