#ifndef MATRIX_EXCEPTION_INCLUDED
#define MATRIX_EXCEPTION_INCLUDED
#include <iostream>
#include "MatrixExceptionNames.cpp"

using namespace std;

class MatrixException:public exception
{
public:
    MatrixException(){};
    MatrixException(const string& message): mtxExMessage(message){};

    virtual const char* what() const noexcept
    {
        return mtxExMessage.c_str();
    }

private:
    string mtxExMessage;
};
#endif MATRIX_EXCEPTION_INCLUDED
