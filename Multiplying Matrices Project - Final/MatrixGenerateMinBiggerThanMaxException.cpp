#include "MatrixException.cpp"

using namespace std;

class MatrixGenerateMinBiggerThanMaxException:public MatrixException
{
public:
    MatrixGenerateMinBiggerThanMaxException(): MatrixException(MSG_GENERATE_MIN_BIGGER_THAN_MAX_EXCEPTION){}
};
