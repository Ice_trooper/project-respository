#include <iostream>
#include "Matrix.hpp"
#include "MatrixException.cpp"

using namespace std;

int main()
{
    try{
        Matrix matrix1, matrix2, matrixResult;
        matrix1.readFromFile("matrixFile1.txt");
        matrix2.readFromFile("matrixFile2.txt");
        cout<<endl;
        matrix1.displayMatrix();
        cout<<endl;
        matrix2.displayMatrix();
        cout<<endl;
        matrixResult=matrix1.multiplyBy(matrix2);
        matrixResult.displayMatrix();
        cout<<endl;

        matrix1.multiplyBy(3.6f).displayMatrix();

        matrixResult.writeToFile("matrixFileResult.txt");

        cout<<endl;
        Matrix matrix5;
        unsigned int tab[2]={6,8};
        matrix5.generateMatrixArray(tab, -100, 100);
        cout<<"Generated matrix:"<<endl;
        matrix5.displayMatrix();
    }
    catch(MatrixException& mtxEx)
    {
        cout<<mtxEx.what()<<endl;
    }
    catch(exception& ex)
    {
        cout<<ex.what()<<endl;
    }
    catch(...)
    {
        cout<<"Unexpected exception"<<endl;
    }

    return 0;
}
