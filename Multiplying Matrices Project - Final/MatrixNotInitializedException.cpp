#include "MatrixException.cpp"

using namespace std;

class MatrixNotInitializedException:public MatrixException
{
public:
    MatrixNotInitializedException(): MatrixException(MSG_NOT_INITIALIZED_EXCEPTION){}
};
