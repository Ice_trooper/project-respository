#include <fstream>
#include <iostream>
#include <random>
#include "Matrix.hpp"
#include "MatrixExceptionContainer.hpp"

using namespace std;

bool is_emptyFile(ifstream& file)
{
    return file.peek() == ifstream::traits_type::eof();
}

Matrix::Matrix()
{
    matrixArray=nullptr;
}

Matrix::Matrix(Matrix& mtx)
{
    matrixSize[0]=mtx.matrixSize[0];
    matrixSize[1]=mtx.matrixSize[1];

    createEmptyMatrixArray(matrixSize);

    for(unsigned int i=0; i<matrixSize[0]; i++)
    {
        for(unsigned int j=0; j<matrixSize[1]; j++)
        {
            matrixArray[i][j]=mtx.matrixArray[i][j];
        }
    }
}

Matrix::~Matrix()
{
    deleteMatrixArray();
}

Matrix& Matrix::operator= (const Matrix& mtx)
	{
		matrixSize[0] = mtx.matrixSize[0];
		matrixSize[1] = mtx.matrixSize[1];

		createEmptyMatrixArray(matrixSize);

		for (unsigned int i = 0; i<matrixSize[0]; i++)
		{
			for (unsigned int j = 0; j<matrixSize[1]; j++)
			{
				matrixArray[i][j] = mtx.matrixArray[i][j];
			}
		}

		return *this;
	}

unsigned int* Matrix::getMatrixSize()
{
    return this->matrixSize;
}

void Matrix::setMatrixSize(unsigned int m_x, unsigned int m_y)
{
    if(m_x>0 && m_y>0)
    {
        this->matrixSize[0]=m_x;
        this->matrixSize[1]=m_y;
    }else throw MatrixBadSizeException();
}

void Matrix::displayMatrix()
{
    /*try
    {
        if(matrixArray)
        {
            cout<<"Size: "<<matrixSize[0]<<" "<<matrixSize[1]<<endl;
            for(unsigned int i=0; i<matrixSize[0];i++)
            {
                for(unsigned int j=0; j<matrixSize[1];j++)
                {
                    cout<<matrixArray[i][j]<<"\t";
                }
                cout<<endl;
            }
        }else {throw MatrixNotInitializedException();}; //BLAD
    }
    catch(MatrixNotInitializedException& mtxEx)
    {
        cout<<"Error: Cannot display matrix. "<<mtxEx.what()<<endl;
    }*/
    if(matrixArray)
    {
        cout<<"Size: "<<matrixSize[0]<<" "<<matrixSize[1]<<endl;
        for(unsigned int i=0; i<matrixSize[0];i++)
        {
            for(unsigned int j=0; j<matrixSize[1];j++)
            {
                cout<<matrixArray[i][j]<<"\t";
            }
            cout<<endl;
        }
    }else throw MatrixNotInitializedException();
}

void Matrix::createEmptyMatrixArray(unsigned int mSize[])
{
    if(!matrixArray)
    {
        setMatrixSize(mSize[0], mSize[1]);
        matrixArray = new float*[mSize[0]];

        for(unsigned int i=0; i<mSize[0]; ++i)
        {
            matrixArray[i] = new float[mSize[1]];
        }
    }else throw MatrixAlreadyCreatedException(); //BLAD Error: Delete existing matrix before creating new (use method deleteMatrix())
}

void Matrix::createEmptyMatrixArray(unsigned int m_x, unsigned int m_y)
{
    if(!matrixArray)
    {
        setMatrixSize(m_x, m_y);
        matrixArray = new float*[m_x];

        for(unsigned int i=0; i<m_x; ++i)
        {
            matrixArray[i] = new float[m_y];
        }
    }else throw MatrixAlreadyCreatedException(); //BLAD Error: Delete existing matrix before creating new (use method deleteMatrix())
}

void Matrix::deleteMatrixArray()
{
    if(matrixArray)
    {
        for(unsigned int i=0; i<matrixSize[0]; i++)
        {
            delete[] matrixArray[i];
        }
        delete[] matrixArray;
        matrixArray=nullptr;
    }   //cout<<"MatrixArray nie istnieje"<<endl; //WARNING
}

//Create a matrix and assign randomly generated numbers.
void Matrix::generateMatrixArray(unsigned int mSize[], int minimum, int maximum)
{
    createEmptyMatrixArray(mSize);

    if(minimum>=maximum) throw MatrixGenerateMinBiggerThanMaxException();

    random_device rd;
    mt19937 mt(rd());
    uniform_int_distribution<int> dist(minimum, maximum);

    for(unsigned int i=0; i<mSize[0]; i++)
    {
        for(unsigned int j=0; j<mSize[1]; j++)
        {
            matrixArray[i][j]=dist(mt);
            //matrixArray[i][j]=minimum + static_cast <int> (rand()) /( static_cast <int> (RAND_MAX/(maximum-minimum)));;
        }
    }
}

//Create a matrix and assign randomly generated numbers.
void Matrix::generateMatrixArray(unsigned int mSize[], float minimum, float maximum)
{
    createEmptyMatrixArray(mSize);

    if(minimum>=maximum) throw MatrixGenerateMinBiggerThanMaxException();

    random_device rd;
    mt19937 mt(rd());
    uniform_real_distribution<float> dist(minimum, maximum);

    for(unsigned int i=0; i<mSize[0]; i++)
    {
        for(unsigned int j=0; j<mSize[1]; j++)
        {
            matrixArray[i][j]=dist(mt);
            //matrixArray[i][j]=minimum + static_cast <float> (rand()) /( static_cast <float> (RAND_MAX/(maximum-minimum)));;
        }
    }
}

void Matrix::readFromFile (string fileName)
{
    ifstream matrixFile;
    matrixFile.open(fileName.c_str());

    if(matrixFile.is_open()) //BLAD Cannot open file.
    {
        if(is_emptyFile(matrixFile))
        {
            cout<<fileName<<" - ";
            throw MatrixFileIsEmptyException(); //BLAD File is empty
        }

        unsigned int mSize[2];
        int mRows, mCols;
        if(!(matrixFile>>mRows && matrixFile>>mCols))
        {
            if(matrixFile.eof())
            {
                throw MatrixCannotReadEntireFileException();
            }
            throw MatrixBadReadSizeException();
        }

        mSize[0]=mRows<0 ? mRows*-1 : mRows;
        mSize[1]=mCols<0 ? mCols*-1 : mCols;

        createEmptyMatrixArray(mSize);

        int allLoops=mSize[0]*mSize[1];
        int loops=0;

        for(unsigned int i=0; i<mSize[0]; i++)
        {
            for(unsigned int j=0; j<mSize[1]; j++)
            {
                if(matrixFile.eof())
                {
                    goto g_eof;
                }

                if(!(matrixFile>>matrixArray[i][j]))
                {
                    throw MatrixBadReadMatrixValuesException();
                }
                loops++;
            }
        }

        g_eof:
        //cout<<allLoops<<" "<<loops<<endl;
        if(allLoops>loops)
        {
            cout<<fileName<<" - ";
            throw MatrixCannotReadEntireFileException(); //BLAD
        }else if(allLoops==loops)
        {
            cout<<fileName<<" - "<<"Success: Read entire file"<<endl;
        }/*else
        {
            cout<<fileName<<" - "<<"Unexpected error #1: Please contact with support. Write down this number of error."<<endl; //BLAD
            return false;
        }*/
        matrixFile.close();

    }else throw MatrixCannotOpenFileException();
}

void Matrix::writeToFile(string fileName)
{
    if(matrixArray)
    {
        ofstream matrixFile;
        matrixFile.open(fileName.c_str());

        if(matrixFile.is_open())
        {
            matrixFile<<matrixSize[0]<<" "<<matrixSize[1]<<endl;

            for(unsigned int i=0; i<matrixSize[0];i++)
            {
                for(unsigned int j=0; j<matrixSize[1];j++)
                {
                    matrixFile<<matrixArray[i][j]<<" ";
                }
                matrixFile<<endl;
            }
        }else throw MatrixCannotOpenFileException();
    }else
    {
        cout<<fileName<<" - ";
        throw MatrixNotInitializedException(); //BLAD
    }
}

//A(nXm)*B(mXp)=C(nXp)
Matrix Matrix::multiplyBy(Matrix& matrix) //enum=0
{
    Matrix matrixResult;
    if(matrixArray && matrix.matrixArray)
    {
        matrixResult.createEmptyMatrixArray(matrixSize[0], matrix.matrixSize[1]);

        if(matrixSize[1]==matrix.matrixSize[0])
        {
            for(unsigned int i=0; i<matrixSize[0]; i++)
            {
                for(unsigned int j=0; j<matrix.matrixSize[1]; j++)
                {
                    float sum=0;
                    for(unsigned int k=0; k<matrixSize[1]; k++)
                    {
                        sum+=matrixArray[i][k]*matrix.matrixArray[k][j];
                    }
                    matrixResult.matrixArray[i][j] = sum;
                }
            }
        }else throw MatrixMultiplyFormatException();
    }else throw MatrixMultiplyEmptyException();

    return matrixResult;
}

//A(nXm)*number
Matrix Matrix::multiplyBy(float number) //enum=0
{
    Matrix matrixResult;
    if(matrixArray)
    {
        matrixResult.createEmptyMatrixArray(matrixSize[0], matrixSize[1]);
        for(unsigned int i=0; i<matrixSize[0]; i++)
        {
            for(unsigned int j=0; j<matrixSize[1]; j++)
            {
                matrixResult.matrixArray[i][j] = matrixArray[i][j]*number;
            }
        }
    }else throw MatrixMultiplyEmptyException();

    return matrixResult;
}
