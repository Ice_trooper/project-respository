#pragma once

#define MSG_NOT_INITIALIZED_EXCEPTION "Error: Cannot use matrix. Matrix is empty."
#define MSG_BAD_SIZE_EXCEPTION "Error: Matrix has a bad size. Size need to be bigger than 0."
#define MSG_ALREADY_CREATED_EXCEPTION "Error: Matrix is already created. Delete that before creating new."
#define MSG_FILE_IS_EMPTY_EXCEPTION "Error: File is empty."
#define MSG_BAD_READ_SIZE_EXCEPTION "Error: Cannot read size of matrix. Check if file have only numbers (not symbols) greater than 0."
#define MSG_BAD_READ_MATRIX_VALUES_EXCEPTION "Error: Cannot read values of matrix. Check if file have only numbers (not symbols)"
#define MSG_CANNOT_READ_ENTIRE_FILE_EXCEPTION "Error: Cannot read entire file"
#define MSG_CANNOT_OPEN_FILE_EXCEPTION "Error: Cannot open file"
#define MSG_MULTIPLY_FORMAT_EXCEPTION "Error: Cannot multiply matrices. Mupltiplied matrices have to got proper format: A(nXm)*B(mXp)."
#define MSG_MULTIPLY_EMPTY_EXCEPTION "Error: Cannot multiply empty matrix."
#define MSG_GENERATE_MIN_BIGGER_THAN_MAX_EXCEPTION "Error: Minimum has to be smaller than maximum"

#define MSG_OUT_OF_RANGE_EXCEPTION "Error: Out of range"

